# Installing

 Clone the repo into a local folder, once cloned `cd` into the exchange directory. You need to have python and pip installed on your local system.

 > git clone git@gitlab.com:indium/exchange.git

 > cd exchange
 
 
Create settings file `cp indium_exchange/settings_example.py indium_exchange/settings.py`

The `settings.py` file contains all the needed tokens. Replace it with necessary tokens.

 > python setup.py develop

 This will install all the needed requirements present in `requirements.txt` using python pip.

## Running
Start the app with the command

 > FLASK_APP=indium_exchange FLASK_DEBUG=1 flask run

or

 > FLASK_APP=indium_exchange python -m flask run

Create database by hitting http://localhost:5000/dbsetup


## TODO
- [ ] Implement order matching.
- [ ] Execute trade by transferring assets over ETH and IPDB.
- [ ] Make it easy to add new bid/ask with payment URIs.
- [x] Polish the UI.
- [ ] Add tests for existing scenarios.
- [ ] Add a contributing guidelines doc.
