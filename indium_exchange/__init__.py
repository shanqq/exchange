from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from web3 import Web3, HTTPProvider
from bigchaindb_driver import BigchainDB
from eth_utils import encode_hex, decode_hex, force_text

from indium_exchange import settings

bdb = BigchainDB(settings.BDB_URL, headers=settings.BDB_TOKENS)
web3 = Web3(HTTPProvider(settings.INFURA_URL))

app = Flask(__name__, template_folder='templates', static_folder='static', static_url_path='')

app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS

db = SQLAlchemy(app)

# db.create_all()

from indium_exchange.api.main import v0
app.register_blueprint(v0)

if __name__ == '__main__':
    app.run(host=settings.SERVER_HOST, port=settings.SERVER_PORT, debug=settings.DEBUG)
