from datetime import datetime

from indium_exchange import db, bdb


class Trade(db.Model):
    __tablename__ = 'trades'
    id = db.Column('trade_id', db.Integer, primary_key=True)
    asset_id = db.Column(db.String(100))
    bid_id = db.Column(db.Integer)
    ask_id = db.Column(db.Integer)
    qty = db.Column(db.Integer)
    price = db.Column(db.Integer)
    ipdb_txn_id = db.Column(db.String(100), unique=True)
    eth_txn_id = db.Column(db.String(100), unique=True)
    timestamp = db.Column(db.DateTime)

    def __init__(self, asset_id, bid_id, ask_id, qty, price, ipdb_txn_id, eth_txn_id):
        self.asset_id = asset_id
        self.bid_id = bid_id
        self.ask_id = ask_id
        self.qty = qty
        self.price = price
        self.ipdb_txn_id = ipdb_txn_id
        self.eth_txn_id = eth_txn_id
        self.timestamp = datetime.utcnow()

    @classmethod
    def match(cls):
        # Keep matching best bids and best asks (price-time priority) as long as it's possible
        while is_trade_possible(): # prices of best bid and ask overlap or match
            best_bid = get_best_bid()
            best_ask = get_best_ask()
            matched_quantity = Math.min(best_bid.qty, best_ask.qty)
            if best_bid.price < best_ask.price:
                break
            else:
                trade_price = [best_bid, best_ask].sort(key=lambda x: x.timestamp).first.price # older order's price becomes the trade price
            create_trade(best_bid, best_ask, qty, trade_price) # Create entry in Trade model and transfer assets in background

    @classmethod
    def create_trade(cls, bid, ask, qty, price):
        trade = cls()
        db.session.add(trade)
        db.session.commit()
        if qty < bid.qty:
          #TODO need to create new bid for the remaining quantity
          bid = Bid()
        elif qty < ask.qty:
          #TODO create new ask for the remaining quantity
          ask = Ask()
          db.session.add(ask)
          db.session.commit()
        return trade

    @classmethod
    def get_best_bid(cls):
      #TODO Select the bid which is open, has the highest price and oldest timestamp
      return None

    @classmethod
    def get_best_ask(cls):
      #TODO Select the ask which is open, has the lowest price and oldest timestamp
      return None

    @classmethod
    def is_trade_possible(cls)
        best_bid = get_best_bid()
        best_ask = get_best_ask()
        if best_bid is None or best_ask is None:
          return False
        return best_bid.price >= best_ask.price