from datetime import datetime

from indium_exchange import db, bdb


class Ask(db.Model):
    __tablename__ = 'asks'
    id = db.Column('ask_id', db.Integer, primary_key=True)
    asset_id = db.Column(db.String(100))
    ipdb_txn_id = db.Column(db.String(100), unique=True)
    qty = db.Column(db.Integer)
    price = db.Column(db.Integer)
    seller_ipdb_address = db.Column(db.String(100))
    seller_eth_address = db.Column(db.String(100))
    timestamp = db.Column(db.DateTime)

    def __init__(self, asset_id, ipdb_txn_id, qty, price, seller_ipdb_address,
                 seller_eth_address):
        self.asset_id = asset_id
        self.ipdb_txn_id = ipdb_txn_id
        self.qty = qty
        self.price = price
        self.seller_ipdb_address = seller_ipdb_address
        self.seller_eth_address = seller_eth_address
        self.timestamp = datetime.utcnow()

    @classmethod
    def from_ipdb_txn(cls, ipdb_txn_id):
        # Query IPDB to get this txn, get values of all columns and create a new Ask row
        status = bdb.transactions.status(ipdb_txn_id)
        if status['status'] == 'valid':
            tx = bdb.transactions.retrieve(ipdb_txn_id)
            ask = cls(tx['asset']['id'], ipdb_txn_id, tx['outputs'][0]['amount'], tx['metadata']['p'],tx['inputs'][0]['owners_before'][0],tx['metadata']['e'])
            db.session.add(ask)
            db.session.commit()
            return ask
        else:
            return None
