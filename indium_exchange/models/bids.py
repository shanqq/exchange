from datetime import datetime

from indium_exchange import db, web3
from eth_utils import encode_hex, decode_hex, force_text
import json

class Bid(db.Model):
    __tablename__ = 'bids'
    id = db.Column('bid_id', db.Integer, primary_key=True)
    asset_id = db.Column(db.String(100))
    eth_txn_id = db.Column(db.String(100), unique=True)
    qty = db.Column(db.Integer)
    price = db.Column(db.Integer)
    buyer_ipdb_address = db.Column(db.String(100))
    buyer_eth_address = db.Column(db.String(100))
    timestamp = db.Column(db.DateTime)

    def __init__(self, asset_id, eth_txn_id, qty, price, buyer_ipdb_address,
                 buyer_eth_address):
        self.asset_id = asset_id
        self.eth_txn_id = eth_txn_id
        self.qty = qty
        self.price = price
        self.buyer_ipdb_address = buyer_ipdb_address
        self.buyer_eth_address = buyer_eth_address
        self.timestamp = datetime.utcnow()

    @classmethod
    def from_eth_txn(cls, eth_txn_id):
         # Query ETH blockchain to get this txn, get values of all columns and create a new Bid row
         # sample: 0x6767c9f42ba2e1ae81284bae9b7619aca4c9fe920e8ae5b4e34d7718e6b4e3a3
         tx = web3.eth.getTransaction(eth_txn_id)
         data = Bid.decode_data(tx['input'])
         bid = cls(data['a'],eth_txn_id,data['q'],tx['value'] // data['q'], data['i'], data['e'])
         db.session.add(bid)
         db.session.commit()
         return bid

    @classmethod
    def encode_data(cls, asset_id, qty, ipdb_address, eth_address):
        data = {'a': asset_id, 'q': qty, 'i': ipdb_address, 'e': eth_address}
        return encode_hex(json.dumps(data))

    @classmethod
    def decode_data(cls, hex):
        return json.loads(force_text(decode_hex(hex)))

    @classmethod
    def sample_data():
        # 0x7b2261223a202234373235383138383636383633646237306438653261363530623861636239326138666438303164636565646663363730396235353935616639316165303863222c202271223a203130302c202269223a2022424a6e366153695062705a7147755868554b6b39693652315136744a5258467a4247556152534a4534516856222c202265223a2022307832323265316533353539664530313434454546356137316535426138374665373836466231663965227d
        # {'a': '4725818866863db70d8e2a650b8acb92a8fd801dceedfc6709b5595af91ae08c', 'q': 100, 'i': 'BJn6aSiPbpZqGuXhUKk9i6R1Q6tJRXFzBGUaRSJE4QhV', 'e': '0x222e1e3559fE0144EEF5a71e5Ba87Fe786Fb1f9e'}
        return encode_data('4725818866863db70d8e2a650b8acb92a8fd801dceedfc6709b5595af91ae08c', 100, 'BJn6aSiPbpZqGuXhUKk9i6R1Q6tJRXFzBGUaRSJE4QhV', '0x222e1e3559fE0144EEF5a71e5Ba87Fe786Fb1f9e')
